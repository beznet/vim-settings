# Vim Settings

The basic idea behind vim-settings is to sync [Neovim](https://neovim.io) settings in a git repository using [vim-plug](https://github.com/junegunn/vim-plug).
The solution is built with my settings embedded for others to use or fork and customize.
Once setup, any customizations and updates are synced via vim-plug.
In addition, as people fork the repo we can all take from each other and have a nice place to see what others are doing.

### Install
- If you want to customize the settings then fork this repository first
- Install python for Neovim following [this](https://github.com/zchee/deoplete-jedi/wiki/Setting-up-Python-for-Neovim#using-virtual-environments)
- Create/replace [init.vim](init.vim) file, updating [`settings_plugin`](init.vim#L2) to your vim-settings plugin if you forked the repository
    - Open Neovim and run `:e $MYVIMRC` to get the destination
    - You can use the following command to automatically install an existing version
        - `wget -O ~/.config/nvim/init.vim https://gitlab.com/flipxfx/vim-settings/raw/master/init.vim`
- Open Neovim and it should install everything automatically

### Notes
- My setup uses [Neovim](https://neovim.io), [iTerm2](https://iterm2.com), [Operator Mono Book/Italic](https://www.typography.com/fonts/operator/overview/) for monospace font, and [Hack Nerd Font](https://nerdfonts.com) for non-ASCII font
- Since I use `nvim` in iTerm2, my fonts are set in iTerm2 and not Neovim
- My spell file is included in this repo
- `<leader>` is set to space

### Make Update
- You will need to have forked vim-settings to do this
- Make changes to vim-settings
    - You can open the vim-settings settings file with `<leader>rc`
- Run `<leader>rC` to refresh with any changes you make
- If you add/remove plugins run `:PlugInstall` or `:PlugClean` respectively, and `<leader>rC` again to refresh
- Commit and push the changes via git
    - Use [vim-fugitive](https://github.com/tpope/vim-fugitive) for quicker results

### Pull Update
- Run `:PlugUpdate`
- Run `<leader>rC` to refresh with changes
- If new plugins are added to [settings.vim](settings.vim), then run `:PlugInstall`, and `<leader>rC`.
