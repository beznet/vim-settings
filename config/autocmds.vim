" File type settings
augroup FileTypeSettings
  autocmd!

  " Markdown
  autocmd BufNewFile,BufReadPost *.markdown,*.md setlocal filetype=markdown shiftwidth=4 softtabstop=4 spell

  " RVM .ruby-env
  autocmd BufNewFile,BufReadPost *.ruby-env setlocal filetype=sh

  " JSON RC files
  autocmd BufNewFile,BufReadPost .babelrc,.stylelintrc setlocal filetype=json

  " Conf files
  autocmd BufNewFile,BufReadPost .dockerignore setlocal filetype=conf

  " Turn off automatic comment inserting for all files
  autocmd Filetype * setlocal formatoptions-=cro
augroup END

" React in Javascript files
augroup JavascriptLibraries
  autocmd!
  autocmd BufReadPre *.js let b:javascript_lib_use_react=1
augroup END

" Fix for reloading files when changed outside Neovim
augroup ReloadFiles
  autocmd!
  autocmd BufEnter,FocusGained * checktime
augroup END

" Git gutter colors
augroup GitGutterColors
  autocmd!
  highlight! link GitGutterChange GruvboxYellowSign
  highlight! link GitGutterChangeDelete GruvboxPurpleSign
augroup END
