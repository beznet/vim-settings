" Airline tab bar
Plug 'vim-airline/vim-airline'

" File system explorer
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" Automatic commenting
Plug 'scrooloose/nerdcommenter'

" Coc autocompletion
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install() } }

" Git wrapper
Plug 'tpope/vim-fugitive'

" Mappings to manipulate surroundings
Plug 'tpope/vim-surround'

" Live replace previews
Plug 'osyo-manga/vim-over', { 'on': 'OverCommandLine' }

" Easy tables
Plug 'godlygeek/tabular', { 'for': 'markdown' }

" Delete buffers except current
Plug 'schickling/vim-bufonly', { 'on': 'BufOnly' }

" Better gf for node
Plug 'tomarrell/vim-npr'

" Gutter git status
Plug 'airblade/vim-gitgutter'

" Git branch viewer
Plug 'rbong/vim-flog'

" Auto-close
Plug 'Raimondi/delimitMate'

" Indent guides
Plug 'nathanaelkane/vim-indent-guides'

" Easier buffer deletion
Plug 'mhinz/vim-sayonara', { 'on': 'Sayonara' }

" HTML5
Plug 'othree/html5.vim', { 'for': ['html', 'vue'] }

" Javascript
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx', 'vue'] }

" Javascript libraries
Plug 'othree/javascript-libraries-syntax.vim', { 'for': ['javascript', 'javascript.jsx'] }

" JSX
Plug 'chemzqm/vim-jsx-improve', { 'for': ['javascript', 'javascript.jsx'] }

" Markdown
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }

" Pug
Plug 'digitaltoad/vim-pug', { 'for': ['pug', 'vue'] }

" Coffeescript
Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' }

" GraphQL files
Plug 'jparise/vim-graphql', { 'for': ['javascript', 'javascript.jsx', 'graphql'] }

" Gruvbox
Plug 'morhetz/gruvbox'

" Devicons for tabs/buffers/NERDTree etc.
Plug 'ryanoasis/vim-devicons'

" Call plug end
call plug#end()
